'use strict';

/**
 * @ngdoc function
 * @name evplaylistsApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the evplaylistsApp
 */
 
angular.module('evplaylistsApp')
  .controller('MainCtrl', function ($scope, $http, $q, Facebook, Page, $location) {
    Page.setTitle('Home');

    $scope.login = function() {
      // From now on you can use the Facebook service just as Facebook api says
      Facebook.login(function(response) {
        // Do something with response.
        if(response.status === 'connected') {
          $scope.logged = true;
        } else {
          $scope.logged = false;
        }
      });
    };

    $scope.getLoginStatus = function() {
      Facebook.getLoginStatus(function(response) {
        if(response.status === 'connected') {
          $scope.logged = true;
        } else {
          $scope.logged = false;
        }
      });
    };

    $scope.eventLink = 'https://www.facebook.com/events/596649333810675';

    $scope.formSubmit = function() {
      var eventId = $scope.getUrlPathInfo(this.eventLink, 'events/');
      $location.path( '/playlist/'+eventId );
    };

    $scope.getUrlPathInfo = function(url) {
      var parser = document.createElement('a');
      parser.href = url;
      
      if (parser.host !== 'facebook.com' && parser.host !== 'www.facebook.com') {
        return false;
      }

      var regex = /(\d+)/g;
      var numbersFromUrl = url.match(regex);
      if (numbersFromUrl[0] === undefined) {
        return false;
      }

      var eventId = numbersFromUrl[0];
      return eventId;
    };

    Facebook.getLoginStatus(function(response) {
      if(response.status === 'connected') {
        $scope.logged = true;
      } else {
        $scope.logged = false;
      }
    });

  });
