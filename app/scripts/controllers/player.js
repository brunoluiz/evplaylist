'use strict';

/**
 * @ngdoc function
 * @name evplaylistsApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the evplaylistsApp
 */

angular.module('evplaylistsApp')
  .controller('PlayerCtrl', function ($scope, $routeParams, $location, $http, $q, Facebook, Youtube, Page, Player, Stats, $interval) {
    $scope.player = Player;
    $scope.eventLink = 'https://www.facebook.com/events/596649333810675';
    $scope.fbEvent = [];
    
    $scope.eventId = $routeParams.id;
    $scope.eventLink = 'https://www.facebook.com/events/'+$scope.eventId;

    function resizeMusicList() {
      var playerHeight = document.getElementsByClassName('col-player')[0].offsetHeight + 'px';

      if (document.body.clientWidth < 991) {
        document.getElementsByClassName('music-list')[0].style.height = '100%';
        return 'mobile';
      }

      document.getElementsByClassName('music-list')[0].style.height = playerHeight;
      return 'desktop';
    }

    $scope.init = function() {
      // Protect the music-list agains copies
      document.getElementsByClassName('music-list')[0].onselectstart = function() { 
        return false;
      };

      Player.reset();

      Facebook.api('/'+$scope.eventId+'?fields=id,name', function(response) {
        $scope.fbEvent.id    = response.id;
        $scope.fbEvent.title = response.name;
        Page.setTitle('Playlist: ' + $scope.fbEvent.title);
      });

      Facebook.api('/'+$scope.eventId+'/feed?fields=id,link,likes.limit(0).summary(true),from&limit=1000', function(response) {
          if (response.error !== undefined) {
            $scope.loading = false;
            $scope.error.status = true;
            $scope.error.noevent = true;

            return false;
          }

          angular.forEach(response.data, function(post) {
            var youtubeID = Youtube.getId(post.link);
            var ytidPos = Player.musics.map(function(e) { return e.ytid; });

            if (youtubeID !== false) {
              // Check if this youtube id didn't exists on the actual list
              var pos = ytidPos.indexOf(youtubeID);
              
              // If it exists, just sum up the 'rank' and save at the actual list item
              if (pos !== -1) {
                Player.musics[pos].rank = Player.musics[pos].rank + post.rank;
              } else {
                post.ytid = youtubeID;
                post.rank = post.likes.summary.total_count;
                Player.musics.push(post);
              }

            }
          });

          if ( Player.musics.length === 0 ) {
            $scope.loading = false;
            $scope.error.status = true;
            $scope.error.nocontent = true;
            return false;
          }

          // Push all needed requests to a promisses array
          var promisses = [];
          var ytIdStr = '';
          var idIdx = 0;
          angular.forEach(Player.musics, function(post) {
            if(idIdx === 20){
              ytIdStr = ytIdStr.substr(1);
              promisses.push( Youtube.getData(ytIdStr) );
              ytIdStr = '';
              idIdx = 0;
            }

            ytIdStr = ytIdStr + ',' + post.ytid;
            idIdx++;
          });
          ytIdStr = ytIdStr.substr(1);
          promisses.push( Youtube.getData(ytIdStr) );

          // Execute all the promisses and then start to process it
          $q.all(promisses).then(function(ytData){
            var fbPostsPos = Player.musics.map(function(e) { return e.ytid; });

            var limboList = [];
            angular.forEach(ytData, function(request) {
              var data = request.data.items;

              angular.forEach(data, function(video) {

                if(video !== undefined) {
                  var ytid = video.id;
                  var pos  = fbPostsPos.indexOf(ytid);

                  Player.musics[pos].title    = video.snippet.title.replace(/\s*\(.*?\)\s*/g, '').replace(/\s*\[.*?\]\s*/g, '');
                  Player.musics[pos].duration = Youtube.getTime(video.contentDetails.duration);
                  Player.musics[pos].ytdata   = video;
                  
                  if (video.status.embeddable === false) {
                    limboList.push({position: pos});
                  }
                }

              });
            });

            angular.forEach(limboList, function(item) {
              Player.musics.splice(item.position,1);
            });
          }).then(function() {
            // Sort the music list
            Player.musics.sort(function(a,b) {
              // Convert NaN values to 0
              a.rank = parseInt(a.rank) || 0;
              b.rank = parseInt(b.rank) || 0;

              if (b.rank > a.rank) { return 1;  }
              if (b.rank < a.rank) { return -1; }
              return 0;
            });
          }).then(function(){
            // Finish the process, loading the UI
            $scope.loading = false;

            resizeMusicList();

            Player.ytVars.autoplay = 1;
            Player.music.pos = 0;
            Player.play();

            Stats.registerPlaylist($scope.fbEvent.id, $scope.fbEvent.title);
          });
      });
    };

    $scope.formSubmit = function() {
      var eventId = $scope.getUrlPathInfo(this.eventLink);
      $location.path( '/playlist/'+eventId );
    };

    $scope.getUrlPathInfo = function(url) {
      var parser = document.createElement('a');
      parser.href = url;
      
      if (parser.host !== 'facebook.com' && parser.host !== 'www.facebook.com') {
        return false;
      }

      var regex = /(\d+)/g;
      var numbersFromUrl = url.match(regex);
      if (numbersFromUrl[0] === undefined) {
        return false;
      }
      

      var eventId = numbersFromUrl[0];
      return eventId;
    };

    $scope.playerTooglePlay = function() {
      if (Player.tooglePlay() === false) {
        $scope.ytPlayer.pauseVideo();
      } else {
        $scope.ytPlayer.playVideo();
      }
    };
    $scope.playerNext = function() {
      Player.forward();
      $scope.ytPlayer.playVideo();
    };
    $scope.playerPrevious = function() {
      Player.backward();
      $scope.ytPlayer.playVideo();
    };
    $scope.playerToogleShuffle = function() {
      $scope.shuffle = Player.toogleShuffle();
    };

    $scope.listItemClick = function(music, pos) {
      Player.music.pos  = pos;
      Player.music.ytid = music.ytid;

      $scope.setNewListItemPos(); // TODO: MAKE IT WORK
      $scope.ytPlayer.playVideo();
    };

    // Play the next item
    $scope.$on('youtube.player.ended', function () {      
      $scope.playerNext();
    });
    $scope.$on('youtube.player.ready', function () {
      $scope.setNewListItemPos();
      Player.play();
      $scope.ytPlayer.playVideo();
    });
    $scope.$on('youtube.player.playing', function () {
      Player.music.title = Player.musics[Player.music.pos].title;
      Player.play();
    });
    $scope.$on('youtube.player.paused', function () {
      Player.stop();
    });

    $scope.setNewListItemPos = function() {
      var activeItem = document.getElementsByClassName('list-group-item active');
      if (activeItem.length === 0) {
        return false;
      }

      var listItemPos = activeItem[0].offsetTop;
      document.getElementsByClassName('music-list')[0].scrollTop = listItemPos;
    };

    $scope.getLoginStatus = function() {
      Facebook.getLoginStatus(function(response) {
        if(response.status === 'connected') {
          $scope.logged = true;
        } else {
          $scope.logged = false;
        }
      });
    };

    $scope.$watch(function() {
      // This is for convenience, to notify if Facebook is loaded and ready to go.
      return Facebook.isReady();
    }, function() {
      // You might want to use this to disable/show/hide buttons and else
      $scope.logged = true;
    });

    $scope.logged  = false;
    $scope.loading = true;
    $scope.error = [];
    $scope.error.status = false;
    Facebook.getLoginStatus(function(response) {
      if(response.status === 'connected') {
        $scope.logged = true;
        $scope.init();
      } else {
        $scope.logged = false;
        $location.path('/');
      }
    });

    $interval(function() {
      if ( ($scope.ytPlayer.getVideoLoadedFraction() === 0) && Player.playing === true) {
        $scope.ytPlayer.playVideo();
      }
    }, 500)
    $interval(function() {
      resizeMusicList();
    }, 1000);
  });
