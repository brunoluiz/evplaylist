'use strict';

/**
 * @ngdoc overview
 * @name evplaylistsApp
 * @description
 * # evplaylistsApp
 *
 * Main module of the application.
 */
angular
  .module('evplaylistsApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'facebook',
    'youtube-embed',
    'parse-angular'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        title: 'Home',
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/playlist/:id', {
        title: 'Playlist',
        templateUrl: 'views/player.html',
        controller: 'PlayerCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  }).run(function () {
    Parse.initialize("2I0ZZoNk3aIzvykQAXJmWPdgFAaAbNqR5JVqHHnO", "CQSCVRUSxZsX0oe5beUyJLias8EN8Q46YEFsGgyw");
  });
