'use strict';

/**
 * @ngdoc service
 * @name evplaylistsApp.Config
 * @description
 * # Config
 * Constant in the evplaylistsApp.
 */
angular.module('evplaylistsApp')
  .constant('configs', {
    appName: 'EvPlaylist',
    appVersion: '0.1 Beta',
    keywords: 'facebook, playlist, music, events',
    fbid: '703746293090635',
    devmode: true
  })
  .config(function(FacebookProvider) {
     // Set your appId through the setAppId method or
     // use the shortcut in the initialize method directly.
     FacebookProvider.init('703746293090635');
  });