'use strict';

/**
 * @ngdoc service
 * @name evplaylistsApp.Player
 * @description
 * # Player
 * Service in the evplaylistsApp.
 */
angular.module('evplaylistsApp')
  .service('Player', function () {
    this.musics = [];

    this.music = {
      pos: 0,
      ytid: '',
      title: '',
      band: ''
    };
    this.playing = false;
    this.shuffle = false;

    this.ytVars = {
        controls: 1,
        disablekb: 0,
        iv_load_policy: 3,
        modestbranding: 1,
        showinfo: 0,
        autoplay: 0
    };

    this.reset = function() {
      this.musics = [];
      this.music = {
        pos: 0,
        ytid: '',
        title: '',
        band: ''
      };
      this.playing = false;
      this.shuffle = false;
    };
    this.toogleShuffle = function() {
      this.shuffle = !this.shuffle;
      return this.shuffle;
    };
    this.tooglePlay = function() {
      this.playing = !this.playing;
      return this.playing;
    };
    this.forward = function() {
      if(this.shuffle === true) {
        this.music.pos = Math.floor((Math.random() * this.musics.length) + 1);
      }
      else {
        this.music.pos++;
      }

      if (this.music.pos >= this.musics.length) {
        this.music.pos = 0;
      }

      this.play();
    };
    this.backward = function() {
      this.music.pos--;
      if (this.music.pos < 0) {
        this.music.pos = 0;
      }

      this.play();
    };
    this.play = function() {
      this.music.ytid = this.musics[this.music.pos].ytid;
      this.playing = true;
    };
    this.stop = function() {
      this.playing = false;
    };
  });
