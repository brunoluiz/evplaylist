'use strict';

/**
 * @ngdoc service
 * @name evplaylistsApp.Page
 * @description
 * # Page
 * Factory in the evplaylistsApp.
 */
angular.module('evplaylistsApp')
  .factory('Page', function (configs) {
    // Service logic
    // ...

    var title_ = configs.appName;

    // Public API here
    return {
      getTitle: function() { return title_; },
      setTitle: function(newTitle) { 
        document.title = configs.appName + ' - ' + newTitle; 
      }
    };
  });
