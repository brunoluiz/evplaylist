'use strict';

/**
 * @ngdoc service
 * @name evplaylistsApp.Youtube
 * @description
 * # Youtube
 * Service in the evplaylistsApp.
 */

angular.module('evplaylistsApp')
  .service('Youtube', function($http) {

    // Private functions
    function leadingZeroes(number, size) {
      var str = number+'';
      
      while (str.length < size){
        str = '0' + str;
      }

      return str;
    }

    function secondsToTime(secs) {
      var hours = Math.floor(secs / (60 * 60));
     
      var divisorForMinutes = secs % (60 * 60);
      var minutes = Math.floor(divisorForMinutes / 60);
   
      var divisorForSeconds = divisorForMinutes % 60;
      var seconds = Math.ceil(divisorForSeconds);
     
      var obj = {
          'hours':    leadingZeroes(hours, 2),
          'minutes':  leadingZeroes(minutes, 2),
          'seconds':  leadingZeroes(seconds, 2)
      };
      return obj;
    }


    // Public functions
    this.getId = function(url) {
      var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;

      if(url === undefined) {
        return false;
      }

      return (url.match(p)) ? RegExp.$1 : false ;
    };

    this.getTime = function(timeStr) {
      var reptms = /^PT(?:(\d+)H)?(?:(\d+)M)?(?:(\d+)S)?$/;
      var hours = 0, minutes = 0, seconds = 0, totalseconds;

      if (reptms.test(timeStr)) {
        var matches = reptms.exec(timeStr);
        
        if (matches[1]) {
          hours = Number(matches[1]);
        }
        if (matches[2]) {
          minutes = Number(matches[2]);
        }
        if (matches[3]) {
          seconds = Number(matches[3]);
        }

        totalseconds = hours * 3600  + minutes * 60 + seconds;
      }

      return secondsToTime(totalseconds);
    };

    this.getData = function(id) {
      var apiKey = 'AIzaSyCJf9YsNK-d7MvMPg-B_Ov-awo10z816Qw';
      var url = 'https://www.googleapis.com/youtube/v3/videos?'+
      'id='+id+
      '&key='+apiKey+
      '&maxResults='+50+
      '&part=snippet,contentDetails,status';

      return $http.get(url);
    };
  });