'use strict';

/**
 * @ngdoc service
 * @name evplaylistsApp.Stats
 * @description
 * # Stats
 * Service in the evplaylistsApp.
 */
angular.module('evplaylistsApp')
  .service('Stats', function (configs) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    this.registerPlaylist = function(id, name) {
      if (configs.devmode === true) {
        return true;
      }

      var PlaylistHistoricObject = Parse.Object.extend("PlaylistHistoricObject");
      var playlist = new PlaylistHistoricObject();
      playlist.save( {'event_id': id, 'name': name} );

      return true;
    }

    this.registerSessionTime = function (id, name, time) {
      if (configs.devmode === true) {
        return true;
      }

      var SessionDurationObject = Parse.Object.extend("SessionDurationObject");
      var session = new SessionDurationObject();
      session.save( {'event_id': id, 'name': name, 'duration': time} );

      return true;
    }
  });
